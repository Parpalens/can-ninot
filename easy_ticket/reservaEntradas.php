<?php
/*
* Plugin Name: Reserva entradas
* Description: Plugin para gestionar la compra de entradas.
* Version: 1.0
* Author: Adrián Caballero, Bernat Parpal, Dani Martín
*/

add_filter( 'the_title', 'wprincipiante_cambiar_titulo', 10, 2 );
function wprincipiante_cambiar_titulo( $title, $id ) {
  $title = '[Exclusiva] ' . $title;
  return $title;
}